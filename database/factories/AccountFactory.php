<?php

use Faker\Generator as Faker;

$factory->define(App\Account::class, function (Faker $faker) {
    return [
        //
        'number' => $faker->numberBetween($min = 10000000000, $max = 99999999999),
        'bank_id' => $faker->numberBetween($min = 1, $max = 30),
        'status' =>'alternative',
    ];
});
