<?php

use Faker\Generator as Faker;

$factory->define(App\File::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'status' =>'no_processed',
        'user_id' => $faker->numberBetween($min = 1, $max = 51),
    ];
});
