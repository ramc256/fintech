<?php

use Faker\Generator as Faker;

$factory->define(App\CustomerUser::class, function (Faker $faker) {
    return [
        'customer_id' => $faker->numberBetween($min = 1, $max = 30),
        'user_id' => $faker->numberBetween($min = 1, $max = 51),
        'bank_id' => $faker->numberBetween($min = 1, $max = 30),
       	'phone'   => $faker->phoneNumber,
        'biweekly_salary' => $faker ->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 600000),
       	'acconunt_number' => $faker ->bankAccountNumber,
       	'acconunt_clabe' => $faker ->numberBetween($min = 100000000000000000, $max = 999999999999999999),
       	'status' =>  $faker -> randomElement($array = array ('pending','verifying', 'ready','error')), 
       	'acceptance_terms' => 'no',
        'file_id' => $faker->numberBetween($min = 1, $max = 30),
    ];
});
