<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'customer_user_id' => $faker->numberBetween($min = 1, $max = 30),
        'amount' => $faker->numberBetween($min = 1, $max = 9999999999),
    ];
});
