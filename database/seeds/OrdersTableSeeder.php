<?php

use Illuminate\Database\Seeder;
use App\Order;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {	
    	factory(Order::class)->create([
    		'customer_user_id' => 1,
    		'amount' => 56565.78,
    	]);
        factory(Order::class,30)->create();
    }
}
