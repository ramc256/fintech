<?php

use Illuminate\Database\Seeder;
use App\Bank;
use App\Account;
class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bank = factory(Bank::class)->create(['name' => 'Banco Santander']);
        factory(Account::class,1)->create([
            'bank_id' => $bank->id,
            'status' => 'principal'
        ]);
        factory(Bank::class,30)->create();
    }
}
