<?php

use Illuminate\Database\Seeder;
use App\CustomerUser;

class CustomersUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Customeruser::class)->create([
            'customer_id' => 1,
            'user_id'=> 1, 
            'bank_id' => 1,
            'phone' => 584147478643, 
            'acconunt_number' =>11111111111,
            'biweekly_salary' => 56567.88,
            'acconunt_clabe'=>100000000000000000,
            'status'=>'pending',
            'acceptance_terms' => 'no',
            'file_id' => 1,

        ]);
        factory(CustomerUser::class,50)->create();

    }
}
