<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Customer;
use App\User;
use App\Bank;
use App\File;

class CustomerUser extends Model
{
    protected $fillable = [
        'id','customer_id', 'user_id', 'bank_id','phone','number','biweekly_salary','acconunt_number','acconunt_clabe','status','file_name','acceptance_terms','file_id'
    ];

    
    public function orders ()
    {
        return $this->HasMany(Order::class);
    }

    public function customer ()
    {
        return $this->BelongsTo(Customer::class);
    }

    public function user ()
    {
        return $this->BelongsTo(User::class);
    }

    public function file ()
    {
        return $this->BelongsTo(File::class);
    }

    public function bank ()
    {
        return $this->BelongsTo(Bank::class);
    }

    public function scopeStatus()
    {
        $orders = $this->orders;
        return $orders;
    }

    public function scopeSearch($query, $search,$customer_id)
    {
        
        if ($search != '') {
           $query->where('acconunt_number', 'like', "%$search%")
                  ->orWhere('acconunt_clabe', 'like', "%$search%")   
                 ;
        }
        //->orWhere('acconunt_clabe', 'like', "%$search2%")
        
        if ($customer_id != '') {
          /* $customers = Customer::where('name', 'like', "%$target%")->pluck('id');

           $userIds = CustomerUser::whereIn('customer_id', $customers)->pluck('user_id');

           return $query->whereIn('id', $userIds); */
           Log::info("Valor de customer_id: $customer_id");
           $userIds = CustomerUser::where('customer_id', $customer_id)->pluck('user_id');
           Log::info("Valor de customer_id: " . count($userIds));
           return $query->whereIn('id', $userIds);
        }
    }


}

