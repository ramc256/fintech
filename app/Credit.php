<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{

  protected $fillable = [
        'order_id', 'status',
    ];
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function scopeSearch($query, $search)
    {
        
        if ($search != '') {
           $query->where('status', 'like', "%$search%");
        }
        
    }

}
