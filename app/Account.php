<?php

namespace App;
use App\Bank;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'number','status','bank_id',
    ];

    public function bank()
    {
        return $this->BelongsTo(Bank::class);
    }
    
}
