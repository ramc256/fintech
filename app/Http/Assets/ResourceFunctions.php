<?php

namespace App\Http\Assets;
use Illuminate\Support\Facades\Log;
use Twilio;
use Services_Twilio;
use Twilio\Rest\Client;
use Carbon\Carbon;
use infobip\api\client\SendSingleTextualSms;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest;


class ResourceFunctions 
{

    public static function sendMessage ($number, $message) 
    {
       /* 
        // Create an authenticated client for the Twilio API
        $client = new Services_Twilio($_ENV['TWILIO_ACCOUNT_SID'], $_ENV['TWILIO_AUTH_TOKEN']);
     
            // Use the Twilio REST API client to send a text message
            $m = $client->account->messages->sendMessage(
            $_ENV['TWILIO_NUMBER'], // the text will be sent from your Twilio number
            $number, // the phone number the text will be sent to
            $message // the body of the text message
        );
 
        // Return the message object to the browser as JSON
        return $m;
        */
       
       // Initializing SendSingleTextualSms client with appropriate configuration
      $client = new SendSingleTextualSms(new BasicAuthConfiguration($_ENV['INFOBIP_USERNAME'], $_ENV['INFOBIP_PASSWORD'] ));
      // Creating request imap_body(imap_stream, msg_number)
      $requestBody = new SMSTextualRequest();
      $requestBody->setFrom($_ENV['INFOBIP_FROM']);
      $requestBody->setTo([$number]);
      $requestBody->setText($message);
      // Executing request
      try {
          $response = $client->execute($requestBody);
          $sentMessageInfo = $response->getMessages()[0];
         // echo "Message ID: " . $sentMessageInfo->getMessageId() . "\n";
          //echo "Receiver: " . $sentMessageInfo->getTo() . "\n";
          //echo "Message status: " . $sentMessageInfo->getStatus()->getName();
      } catch (Exception $exception) {
        Log::error("Ocurrio un error: [$exception->getCode()]");
        Log::error("Ocurrio un error: [$exception->getMessage()]"); 

      }

      return $sentMessageInfo->getStatus()->getName();
        
    }

    /**
     * [searchCredit description: method that nusca the credits in process related to the account passed by parameter]
     * @return [type] [description]
     */
    public static function searchCredit($credit, $account_file, $amount_file)
    {
      
      $result_amount =preg_replace("/[^0-9.,]/", "", $amount_file);
      
      foreach ($credit as $key => $value) {
          
        if ( ($account_file=== $value->order->customer_users->acconunt_number) 
              && ($result_amount ==  $value->order->amount) ) {
              return ['id'=>$value->id]; 
        }

      }
      return ['id'=> false];
    }

    public static function validateCredit ($customerUsers, $credit, $credit_amount)
    {
        $total_credit =0;
        $flag=false;
        $available = 0;
        $calculation_percentage =($customerUsers->biweekly_salary * 0.30);
     
        foreach ($customerUsers->orders as $value) {
            
            foreach ($credit as $value_credit) {
                if ($value_credit->credit->status=='pending_processing' && $value_credit->credit->order_id == $value->id ) {
                    $total_credit += $value->amount;
                }
              }
            
        }

        /**
         * 
         */
        $available = (  $calculation_percentage - $total_credit) ;
        if ($available <= 0 ) {
            $available =0.00;
        }
        
        if (($total_credit >= $calculation_percentage ) || ( ( $total_credit + $credit_amount) > $calculation_percentage ) ) {
            $result=[
                'flag'      => true,
                'message'   => 'El monto disponible para solicitar credito es de: '.$available,
                'available' => $available
            ];
            
        }else{ 
            $result=[
                'flag' => false,
                'message' => 'El monto solicitado esta disponible',
                'available' => $available
            ];
        }
        
        return $result;
    }


    public static function addSpaces($ceros,$data,$type, $position)
    {
      $insertar_ceros =null;
      $order_diez = explode(".",$data);
      $dif_diez = $ceros - strlen($order_diez[0]);
      if ($type == 1) {
        $add = 0;
      }else{
        $add = " ";
      }
          //dd($dif_diez);
        for($m = 0 ; $m < $dif_diez;  $m++)
        {
                $insertar_ceros .=$add;
        }

        if ($position=='first') {
          # code...
        return $insertar_ceros.$data;
        }else{
          return $data.$insertar_ceros;
        }

    }

    
    /**
     * [rangeDate description]
     * @return [type] [description]
     */
    public static function rangeDate()
    {
        $date = Carbon::now();

        $date_d = $date->format('d');
        
        if ($date_d <= 15) {
           return array(
                $date->format('Y').'-'.$date->format('m').'-'.'01 00:00:00',
                $date->format('Y').'-'.$date->format('m').'-'.'15 23:59:59'
            );
        }else{
            return array(
                $date->format('Y').'-'.$date->format('m').'-'.'16 00:00:00',
                $date->format('Y').'-'.$date->format('m').'-'.'31 23:59:59'
            );
        }
    }

     public static function messageError($controller, $column, $message)
    {
        \Log::critical($message.':  || '.$controller . '- (), del usuario: ');
        return ['message' => 'La columna bank solo debe poseer valores numericos'];
        //flash('La columna '.$column.' '.$message.' ¡Error!')->error();
    }

    public static function messageErrorColumnNumeric($controller, $column)
    {
        \Log::critical('la columna debe poseer datos numericos: '. ' || '.$controller . '- (), del usuario: ');
        //flash('La Columna '.$column.' debe poseer solo valores numericos', '¡Error!')->error();
    }

    public static function messageStructureHeadboard($controller)
    {
        \Log::critical(' La estructura del archivo no es la correcta ' . ' -----> ['.$controller . ' del usuario: ');
        //flash('La estructura del archivo no es la correcta', '¡Alert!')->warning();
    }

    /**
     * [validateColumnNumeric
     *  Validates that a column has only numerical values ]
     * @param  [arrays] $dataColumn   [Arrangement with the data in column]
     * @param  [arrays] $key   [Column index]
     * @return [boolean] $validate     [Returns true if it has a non-numeric value]
     */
    public static function validateColumnNumeric($dataColumn){
        
        foreach ($dataColumn as $value) {
            if (is_numeric($value)==false ) {
                return true;
            }
        }

        return false;
    }

    /**
     * [validateColumnNumeric
     *  Validates that a column has only numerical values ]
     * @param  [arrays] $dataColumn   [Arrangement with the data in column]
     * @param  [arrays] $key   [Column index]
     * @return [boolean] $validate     [Returns true if it has a non-numeric value]
     */
    public static function validateAccount($dataColumn, $q){
        foreach ($dataColumn as $value) {
            if (strlen($value) != $q ) {
                return true;
            }
        }

        return false;
    }

    public static function validateAccountClabe($dataColumn, $q){
        foreach ($dataColumn as $value) {
            if ( strlen($value) != $q ) {
                if ($value.length!= $q ) {
                    return true;
                }
            }

        return false;
        }
    }


    /**
     * [validateDuplicateValue
     *  Validates that a column has no duplicate values ]
     * @param  [arrays] $array   [Arrangement with the data in column]
     * @return [boolean] $validate     [Returns true if it gets duplicate values]
     */
    public static function validateDuplicateValue($array){
        
        if( (count($array) )!= (count(array_unique($array) )) ){
            Log::error("tamaño: ".(count($array)) ." otro dato: ".(count(array_unique($array) )));
            return true;
        }

        return false;

    }

    /**
     * [validateColumnVoid
     *  Validate that a column has no empty values ]
     * @param  [arrays] $array   [Arrangement with the data in column]
     * @return [boolean] $validate     [Returns true if it gets duplicate values]
     */
    public static function  validateColumnVoid($dataColumn){
      
      $validate=true;
      foreach ($dataColumn as $value) {
            if ( (empty($value)==true) )  {
                $validate = false;

            }

        }
        Log::info('Dentro de validateColumnVoid '.$validate);
      return $validate;
    }


   public static function arraySimple($array)
   {
    $i = 0;
    foreach ($array as $key) {
        $valor[$i] = $key;
        $i++;
    }

    return $valor;
    }


    /**
     * [validateHead
     *  .
     *  Validates the header of the files to import ]
     * @param  [arrays] $keys   [Arrangement with defined keys]
     * @param  [arrays] $data   [Fix with the indexes that the file brings]
     * @return [integet] $i     [Returns in number of valid indexes]
     */
    public static function validateHeadboard($keys, $data){
      $j=0; $i=0;
      $validate=true;
      foreach ($data as $value) {
            foreach ($value as $key => $value2) {
                foreach ($keys as $value3) {
     // Log::info("metodo de validacion iguales: ".$value3.'     '.$value2);
                    if ($key === $value3) {
                        $j++;
                    }
                }
            $i++;
            }
        break;
        }

        if(  !((count($keys) == $j) &&  ( count($keys)==$i ))   ){
            $validate = false;

       }

      return $validate;

    }

}
