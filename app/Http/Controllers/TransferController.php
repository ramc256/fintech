<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Assets\ResourceFunctions;
use Illuminate\Support\Collection as Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Credit;
use App\Account;
use App\CustomerUser;
use App\HistoricalImport;
use Auth;
use Excel;

class TransferController extends Controller
{

	public function index()
    {

        return view('admin.transfer-file.index'); 
    }


	public function listCredits()
	{ 
		$target = request()->search;
	    Log::info('Dentro del controlador: ' .$target);
	    
	    if ($target=='0'){
            $target = 'pending_procesing';
        }else if ($target=='1') {
            $target = 'processing';
        }else if ($target == '2') {
            $target = 'error';
        }else{
            $target = 'ready';
        }

	    return [
		  'credits' => Credit::with(['order.customer_users.bank','order.customer_users.user','order.customer_users.customer'])
    						->search( $target)
	     					->orderBy('id', 'desc')
	     					->paginate()	
		]; 
	}

	public function importFileTransfer()
	{

			$file = fopen(request()->file, "r");
			$i=0;
			/*
			*while there is a row
			*/
			while(!feof($file)){
				/*
				*method that allows to capture each line of the file
				*/
				$linea = fgets($file,1000);
				/*
				*generate array via the separator "|" which is the one with the txt file
				*/
				$data_file[$i]= explode(';',trim($linea));
				

				$i++;
			}//while close
			/*
			*arrangement with the header that the file must have
			*/
			$validator = [
				0=> 3, 
        		1=> 8 
			];

			$names_key=[
				'account_number'=> 3,
				'status' => 8
			];
				
				$r=0;
			foreach ($data_file as $key => $value) {
				if (isset($value['3']) ) {
				Log::info('informacion al recorrer el arrreglo'.$value['0']);
					$data[$r]=[
						'account_number' => trim($value['3']),
						'amount'		  => trim($value['5']),
						'status'		  => trim($value['8'])			
					];
				}
				$r++;
				

			}
			
			//return['message'=> $data];	
				/*
				*deletes the last record of the file, this record is blank
				*/
				
				//return ResourceFunctions::validationFile ($data, $names_key, $validator, $objetives, $file_name);
			
		Log::info('Dentro del controlador TransferController@importFileTransfer: ');

        $historical_import = HistoricalImport::where('name',request()->loadFileName)->first();

        $keys=[
            0=> 3, 
            1=> 8, 

        ];
    	if (count($historical_import)>0) {
    		return[
            'message' => 'Este archivo ya ha sido procesado',
            'status'  => null
        	];	
    	
    	}else if ((empty($data )) || (count($data)==0)) {
            
            return [  
                'message' => 'El archivo que cargo no posee datos',
                'status'  => null
            ];

        }else if ( ResourceFunctions::validateHeadboard($keys, $data)) {
            /*
            *Validate that the file header is correct
            */
            return [
            	'message' => 'La estructura del archivo no es la correcta',
       			'status'  => null
       		];
            
        }else if (ResourceFunctions::validateColumnVoid( array_column($data, 'account_number') )==false) {
            /*
            *Validate that the account_number column is not void
            */
            return [  
                'message' => 'La columna ACCOUNT_NUMBER no debe tener campos vacios',
                'status'  => null
            ];
        }else if (ResourceFunctions::validateColumnVoid( array_column($data, 'amount') )==false) {
            /*
            *Validate that the PHONE column is not void
            */ 
            return[
                'message' => 'La columna RESULT no debe tener campos vacios',
                'status'  => null
            ];
        }else{
        	/**
        	 * [$customer_users description: querry of all account number]
        	 * @var [type:]
        	 */
        	$customer_users = CustomerUser::select('*')->get()->toArray();
        	/**
        	 * [$credit description: querry of credits that are being processed]
        	 * @var [type]
        	 */
        	$credits = Credit::with('order.customer_users')->where('status','processing')->get();

        	try {
                DB::beginTransaction();

	        	foreach ($data as $key => $valueData) {

		            //Allows to validate if the user is registered in the database
		                if ( in_array($valueData['account_number'], array_column($customer_users, 'acconunt_number') ) ) 
	                    {
	                    	$result =ResourceFunctions::searchCredit($credits, $valueData['account_number'],$valueData['amount'] );
	                    	if ($result['id']!=false) {
		            
		            				$container_credits[] = [
		                                'account_number'    => $valueData['account_number'],
		                                'status'            => $valueData['status'],
		                                'account_status'    => 'procesada',
		                            ];
		                            if ($valueData['status']== 'Aceptado') {
		                            	$v_status = 'ready';
		                            }else{
		                            	$v_status = 'error';
		                            }
		                            Credit::FindOrFail($result['id'])
		        					->update( ['status'=> $v_status]);
	                    	}else{
	                    		$container_credits[] = [
		                            'account_number'    => $valueData['account_number'],
		                            'status'            => $valueData['status'],
		                            'account_status'    => 'transaccion no conseguida',
		                        ];
	                    	}                      
	                    
	                    }else{

	                    	$container_credits[] = [
	                            'account_number'    => $valueData['account_number'],
	                            'status'            => $valueData['status'],
	                            'account_status'    => 'Cuenta no registrada',
	                        ];
	                    }		
	        			
	        	}//close foreach

        	DB::commit();   
            } catch (Exception $e) {
            	Log::error('Ah ocurrido un error en TransferController@importFileTransfer: ' . $e );
            	return ['message' => 'Ocurrio un error al realizar la operacion',
            			'credits' => null,
            			'status'  => null	
            			];
                DB::rollBack();
            }
            return [  
                'message' => 'La operacion se realizo con exíto',
                'credits' => $container_credits,
                'status'  => true
            ];	

        }




	}//cierre del metodo

    public function TransferOtherBanks()
	{

		Log::info("paso por el proceso de generar archivo de transferencia TransferOtherBanks: ".Auth::user()->name);
		$flag=false;

		$credits =Credit::where('status','pending_processing')
    		->with(['order.customer_users.bank','order.customer_users.user'])
    		->get();
    	$bank_accounts = Account::where('status','principal')->first();
    	if (count($bank_accounts) == 0 ) {
    		return [
                'message' => 'Debe existir una cuenta principal de fintech registrada',
                'status' => 0,
                'fileNameOtherBank' => null
            ];
    	}

    	$container_transference = [];
    	
    	if (count($credits)>=1) {
	       
	       $file_name = 'TransferOtherBanks_'.time().'_'. mt_rand().'.txt';
	        $file = fopen($file_name, "w");

	        $folder = public_path() . "/storage/users/" ;

	        
	        if( is_dir($folder) == false )
	        {                   
	            mkdir($folder, 0777, true);
	        }
	               
	        $path = $folder . $file_name;

	       
	        foreach ($credits as $value) {
	       		
    		   	if ($value->order->customer_users->bank_id != $bank_accounts->bank_id ) {

       				$container_transference = [
       					'change_account' => $bank_accounts->number.'     ',
       					'payment_account' => $value->order->customer_users->number.'  ',
       					'receiving_bank' => $value->order->customer_users->bank->transfer_key,
       					'baneficiary'    => ResourceFunctions::addSpaces(40, $value->order->customer_users->user->name, 0,'first'),
       					'sucursal' => 0000,
       					//'amount'  => ResourceFunctions::addSpaces(15,$value->order->amount, 1,'first'),
       					'plaza_banxico' => '     ',
       					//'concepto' => ResourceFunctions::addSpaces(40, 'PRESTAMO FINCTECH', 0,'first'),
       					'status_account' => 's', 
       					'RFC' => '             ', 
       					'iva' => '000000000000,00',
       					'reference_payer' => '       ',
       					'application_form' => '1'."\n", 

       				];
       				$flag = true;
			        file_put_contents($path, $container_transference, FILE_APPEND | LOCK_EX);
			        
       				Credit::FindOrFail($value->id)
        			->update(['status'=> 'processing']);
	       		
		       	}	       			
	        }
	       	fclose($file);
	        
	        if ($flag==true) {
	        	
	        	return [
                    'message' => 'El archivo se genero con exíto dele click al boton descargar',
                    'status' => 1,
                    'fileNameOtherBank' => $file_name
                ];
	        	
	        }else{
	        	return [
                    'message' => 'No hay creditos pendientes por procesar',
                    'status' => 0,
                    'fileNameOtherBank' => null
                ];
	        }
	       
    	}else{

    		return [
                    'message' => 'No hay creditos pendientes por procesar',
                    'status' => 0,
                    'fileNameOtherBank' => null
                ];
    	}
 
	}
}
