<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Banxico;

use Illuminate\Http\Request;

class BanxicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.banxico.index'); 
    }

    public function list()
    {
    	Log::info("metodo de lista: ");
        return Banxico::search(request()->search)->orderBy('id', 'DESC')->paginate();
    }

    public function store()
    {
    	Log::info("store banxico: ".request()->number);
        $banxico = Banxico::create(request()->all());
        $banxico->save();

        return ['message' => 'El registro se realizo con exito'];
    }

    public function show()
    {
        return view('admin.banxico.show') -> with(['id' => request() -> id]);
    }

    public function update($id)
    {
    	Log::info("update banxico: ".$id);
        $banxico = Banxico::find($id);

        $banxico->fill(request()->all());

        $banxico->save();
        
        return ['message' => 'La actualizacion se realizo con exito'];
    }

    public function getData()
    {
       return Banxico::FindOrFail(request()->id);
    }

    public function delete()
    {
    	try {
    		DB::beginTransaction();
    		Log::info("delete banxico: ".request()->number);

        	Banxico::destroy(request()->id);
    		DB::commit();   
        } catch (Exception $e) {
            DB::rollBack();
        }

        return ['message' => 'Se ha eliminado con exito'];
    }
}
