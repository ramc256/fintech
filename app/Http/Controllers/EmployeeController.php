<?php
// codigo de freddy
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Bank;
use App\Customer;
use App\CustomerUser;
use App\Http\Assets\ResourceFunctions;
use Log;
use Excel;
use DB;
use Auth;

class EmployeeController extends Controller
{
    public function index()
    {
        return view('employee.index');
    }

    public function terms()
    {
        return view('terms.index');
    }

    public function list()
    { 
        $target = request()->search;
        $customers = Customer::where('admin_id',auth()->user()->id)->pluck('id');
        // busca el id, de las empresas donde id_admin sea igual al usuario logueado
        $customer_user = CustomerUser::whereIn('customer_id', $customers)->pluck('user_id');// devuelve todos los usuarios que pertenecen a esas empresas del usuario loqueado
        //Log::info('cantidad de employee '.count($customer_user));
        
        $users = User::with('customerUser') // devuelve los datos de la relación  with customer_user
         ->with('banks') // devuelve los datos de la relación  with banks
         ->employeeSearch( $target ) // llamado al scope employeesearch dentro modelo user
         ->where('deleted', false) // deleted false
         ->whereIn('id', $customer_user) // comparando id table user con todo los id de las tabla customer user
         ->orderBy('id', 'desc')
         ->paginate();

        $banks = Bank::all();

        return [
          'users' =>  $users,
          'banks' =>  $banks,
        ]; 
    }

    public function store()
    {
        $user = User::create([
            'rfc' => request()->rfc,
            'name' => request()->name,
            'nacionality' => request()->nacionality,
            'email' => request()->email,
            'password' => bcrypt(request()->password),
            'role' => "empleado",
            'status' => request()->status,
        ]);

        $customer = Customer::where('admin_id', auth()->user()->id)->first();

        $customeruser = CustomerUser::create([
            'customer_id' => $customer->id,
            'user_id' => $user->id,
            'bank_id' => request()->bank_id,
            'phone' => request()->phone,
            'acconunt_number' => request()->acconunt_number,
            'acconunt_clabe' => request()->acconunt_clabe,
            'biweekly_salary' => request()->biweekly_salary        
        ]);

        $customeruser->save();

        return ['message' => 'El empleado se ha creado con exito'];
    }

    public function update($id)
    {
        $user = User::find($id);

        $user->fill(request()->all());

        $user->save();

        $customer_user = CustomerUser::where('user_id', $id)->first();

        $customer_user->update([
            'bank_id' => request()->bank_id,
            'phone' => request()->phone,
            'acconunt_number' => request()->acconunt_number,
            'acconunt_clabe' => request()->acconunt_clabe,
            'biweekly_salary' => request()->biweekly_salary,     
        ]);

        $customer_user -> save();
        
        return ['message' => 'El empleado se ha actualizada con exito'];
    }

    public function show()
    {
        return view('admin.user.show') -> with(['id' => request() -> id]);
    }

    public function getData()
    {
       return User::FindOrFail(request()->id);
    }
     
    public function delete()
    {
        $user = User::find(request()->id); 
        $user -> deleted = true; 
        $user -> save();

        return ['message' => 'El empleado se ha eliminado con exito'];
    }

    protected function saveFile($base64, $name, $work_oder_id)
   {
       $exploded = explode(',', $base64);

       $decoded = base64_decode($exploded[1]);

       $fileName = $name;

       $folder = public_path() . "/storage/work_orders/$work_oder_id/";

       if( is_dir($folder) == false )
       {
           mkdir($folder);
       }

       $path = $folder . $fileName;

       file_put_contents($path, $decoded);
   }

   /**
    * [validateTerms description: method that allows to validate the terms and conditions]
    * @param  [type] $id [description: user identifier that validates the terms and conditions]
    * @return [type]     [description: message that denotes what the result was]
    */
   protected function validateTerms($id)
   {
        Log::info("EmployeeController actualizar estatus terminos y condiciones updateTerms: ".$id);
        try {

            $user = User::FindOrFail($id)
            ->update([
                'status'=> 1
            ]);

            if ($user==1) {
            return['message'=> 'El proceso se relizo con exito',
                    'status' => 1
            ];
            }else{
                return[
                    'message'=> 'El proceso no se realizo',
                    'status' => 0
                ];
            }
            
            }catch(\Exception $e){
                Log::error("Ha ocurrido un error al intentar actualizar el estatus de aceptacion de terminos [$e]");
                return response()->json(['status', 'Error al actulizar el estatus de aceptacion de terminos'], 500);
            }
   }

    // importar archivos por lotes
     public function fileImport(Request $request)
    {
        
        $exploded = explode(',', request()->file);
        
        $decoded = base64_decode($exploded[1]);

        //$fileName = time() . mt_rand() ;

        $fileName = "usuarios.xlsx";

        $folder = public_path() . "/storage/users/" ;
           
        if( is_dir($folder) == false )
        {
                   
            mkdir($folder, 0777, true);
        }
               
        $path = $folder . $fileName;

        //Storage::delete($fileName);
        file_put_contents($path, $decoded);
         
        $data=Excel::load($path)->get();
        /*
        *Indices that validate the file header
        */
        $keys=[
            0=> "rfc", 
            1=> "name", 
            2=> "nacionality",
            3=> "email",
            4=> "bank",
            5=> "phone",
            6=> "biweekly_salary",
            7=> "acconunt_number",
            8=> "acconunt_clabe",

        ];


            if ((empty($data->toArray()) ) || (count($data->toArray())==0)) {
                ResourceFunctions::messageError('UserController',null," El archivo que cargo no posee datos");
                 return [
                        'message' => 'El archivo que cargo no posee datos',
                        'error' => true,
                    ];

                
            }else if ( ResourceFunctions::validateHeadboard($keys, $data->toArray())==false ) {
                /*
                *Validate that the file header is correct
                */
               return [
                        'message' => 'La estructura del archivo no es la correcta',
                        'error' => true,
                    ];
                ResourceFunctions::messageStructureHeadboard('UserController');
                
            }else if (ResourceFunctions::validateColumnVoid( array_column($data->toArray(), 'rfc') )==false) {
                /*
                *Validate that the rfc column is not void
                */
                ResourceFunctions::messageError('UserController',"rfc"," La columna RFC no debe tener campos vacios");

                return [
                        'message' => 'La columna RFC no debe tener campos vacios',
                        'error' => true,
                ];

            }else if ( ResourceFunctions::validateDuplicateValue( array_column($data->toArray(), 'rfc') )  ) {
                Log::error("pase por aqui hora: ".time());
                /*
                *Validate that there are no duplicate values in the column
                */  
                return [ 
                    'message' => 'En el archivo existen RFC duplicados',
                    'error' => true,
                 ];


                
            }else if (ResourceFunctions::validateColumnVoid( array_column($data->toArray(), 'phone') )==false) {
                /*
                *Validate that the PHONE column is not void
                */ 
                ResourceFunctions::messageError('UserController',"phone"," La columna PHONE no debe tener campos vacios");

                 return[ 
                    'message' => 'La columna PHONE no debe tener campos vacios',
                    'error' => true,
                ];
                
            }else if ( ResourceFunctions::validateDuplicateValue( array_column($data->toArray(), 'phone')  )  ) {
                /*
                *Validate that there are no duplicate values in the column
                */   
                return[ 
                    'message' =>'En el archivo existen Phone duplicados',
                    'error' => true,
                ];

                
            }else if (ResourceFunctions::validateColumnVoid( array_column($data->toArray(), 'acconunt_number') )==false) {

                /*
                *Validate that the number column is not void
                */
                ResourceFunctions::messageError('UserController',"account_number"," La columna account_number no debe tener campos vacios");

                return[ 

                    'message' =>'La columna account_number no debe tener campos vacios',
                    'error' => true,
                ];

            }else if (ResourceFunctions::validateColumnVoid( array_column($data->toArray(), 'biweekly_salary') )==false) {
                /*
                *Validate that the biweekly_salary column is not void
                */
                ResourceFunctions::messageError('UserController',"biweekly_salary"," La columna biweekly_salary no debe tener campos vacios");

                return[ 
                    'message' =>'La columna salario quincenal no debe tener campos vacios',
                    'error' => true,
                ];

            }else if (ResourceFunctions::validateColumnVoid( array_column($data->toArray(), 'bank') )==false) {
                /*
                *Validate that the biweekly_salary column is not void
                */
                ResourceFunctions::messageError('UserController',"bank"," La columna bank no debe tener campos vacios");

                 return[ 
                    'message' =>'La columna banco no debe tener campos vacios',
                    'error' => true,
                ];
                
            }else if (ResourceFunctions::validateColumnNumeric( array_column($data->toArray(), 'biweekly_salary') ) ) {
                /*
                *Validate that the biweekly_salary column is just users
                */
                ResourceFunctions::messageError('UserController','biweekly_salary'," La columna biweekly_salary solo debe poseer valores numericos");
               
                return[ 
                    'message' =>'La columna salario quincenal solo debe poseer valores numericos',
                    'error' => true,
                ];

            }else if (ResourceFunctions::validateColumnVoid( array_column($data->toArray(), 'acconunt_clabe') )==false) {
                /*
                *Validate that the biweekly_salary column is not void
                */
                ResourceFunctions::messageError('UserController',"biweekly_salary"," La columna account_clabe no debe tener campos vacios");

                return[ 
                    'message' =>' La columna cuenta clabe no debe tener campos vacios',
                    'error' => true,
            ];

            }else if (ResourceFunctions::validateColumnNumeric( array_column($data->toArray(), 'acconunt_clabe') ) ) {

                return[ 
                    'message' =>'La columna account_clabe no es númerica',
                    'error' => true,
                ];

            }else if (ResourceFunctions::validateColumnVoid( array_column($data->toArray(), 'acconunt_clabe') )==false) {
   //            return ['message' => 'La columna bank solo debe poseer valores numericos'];
                ResourceFunctions::messageError('UserController','acconunt_clabe'," La columna account_clabe solo debe poseer valores numericos");

                return[ 
                    'message' =>'La columna cuenta clabe solo debe poseer valores numericos',
                    'error' => true,
                ];
                                
            }else if (ResourceFunctions::validateAccountClabe( array_column($data->toArray(), 'acconunt_clabe'), 18 ) ) {

                /*
                *Validate that the biweekly_salary column is just users
                */
   //            return ['message' => 'La columna bank solo debe poseer valores numericos'];

                ResourceFunctions::messageError('UserController','account_clabe'," Los registros de la columna account_clabe deben ser cifras de 18 digitos");

                return[ 
                    'message' =>'Los registros de la columna account_clabe deben ser cifras de 18 digitos',
                    'error' => true,
                ];

            }else if (ResourceFunctions::validateAccount( array_column($data->toArray(), 'acconunt_number'), 11 ) ) {
                /*
                *Validate that the biweekly_salary column is just users
                */
   //            return ['message' => 'La columna bank solo debe poseer valores numericos'];
                ResourceFunctions::messageError('UserController','account_number'," Los registros de la columna account_number deben ser cifras de 11 digitos");

                   return[ 
                    'message' =>' Los registros de la columna numero de cuenta deben ser cifras de 11 digitos',
                    'error' => true,
                ];
             

            }else{


                $contenedor_users=[];
                /*
                *query the users 
                */
                $users = User::get()->toArray();
               

               
                /*
                *Consultation of venues the members related to the club
                */
                $customer = CustomerUser::select('customer_id')
                                        ->where('user_id', Auth::user()->id)
                                        ->first();

                $customer_users = CustomerUser::with(['user'])
                    ->where('customer_users.customer_id', $customer->customer_id)
                    ->get()->toArray();
                foreach ($customer_users as $value) {
                        $customer_users [] = $value['user'];                                                                          
                }
                /*
                *querry of phones
                */
                $phones = CustomerUser::select('phone')->get()->toArray();
                /*
                *querry of banks
                */
                $banks = Bank::select('id')->get()->toArray();
                /*
                *Initialize the arrangements in case the query does not bring any value
                */
                if (empty($users)) {
                    $users =[];
                    
                }

                if (empty($customer_users)) {
                    $customer_users=[];                    
                }
                $iteration = 0;
                $container=0;
                Log::info("Antes del primer Foreach");
                foreach ($data as $key => $valueData) 
                {
                     
                    //Allows to validate if the user is registered in the database
                    if ( in_array($valueData->rfc, array_column($users, 'rfc') ) ) 
                    {
                        
                        if ( in_array($valueData->rfc, array_column($customer_users, 'rfc') ) ) 
                        {
  
                            //Saves users who are already related to the customer exist in the db
                          
                            $contenedor_users[] = [
                                'rfc'               => $valueData->rfc,
                                'name'              => $valueData->name,
                                'nacionality'       => $valueData->nacionality,
                                'email'             => $valueData->email,
                                'bank'              => $valueData->bank,
                                'phone'             => $valueData->phone,
                                'biweekly_salary'   => $valueData->biweekly_salary,
                                'acconunt_number'    => $valueData->acconunt_number,
                                'acconunt_clabe'     => $valueData->acconunt_clabe,
                                'line'              => $iteration,
                                'status'            => 'Usuario ya existe'
                            ];
                            $container=1;
                            
                           
                        }else if( !(in_array($valueData->bank, array_column($banks, 'id') )) )
                        {

                            /*
                            *Saves users who have an unsigned site code in the DB
                            */
                            $contenedor_users[] = [
                                'rfc'               => $valueData->rfc,
                                'name'              => $valueData->name,
                                'nacionality'       => $valueData->nacionality,
                                'email'             => $valueData->email,
                                'bank'              => $valueData->bank,
                                'phone'             => $valueData->phone,
                                'biweekly_salary'   => $valueData->biweekly_salary,
                                'acconunt_number'    => $valueData->acconunt_number,
                                'acconunt_clabe'     => $valueData->acconunt_clabe,
                                'line'              => $iteration,
                                'status'            => 'El id del banco no esta en la base de datos'
                            ];
                            $container=1;

                           
                        }else if ( in_array($valueData->phone, array_column($phones, 'phone') ) ){
                            /*
                            *Saves users who have an unsigned site code in the DB
                            */
                            $contenedor_users[] = [
                                'rfc'               => $valueData->rfc,
                                'name'              => $valueData->name,
                                'nacionality'       => $valueData->nacionality,
                                'email'             => $valueData->email,
                                'bank'              => $valueData->bank,
                                'phone'             => $valueData->phone,
                                'biweekly_salary'   => $valueData->biweekly_salary,
                                'acconunt_number'    => $valueData->acconunt_number,
                                'acconunt_clabe'     => $valueData->acconunt_clabe,
                                'line'              => $iteration,
                                'status'            => 'El telefono ya existe en la BD'
                            ];
                            $container=1;

                                        
                            
                        }else{

                            /*
                            *Saves users who have an unsigned site code in the DB
                            */
                            $contenedor_users[] = [
                                'rfc'               => $valueData->rfc,
                                'name'              => $valueData->name,
                                'nacionality'       => $valueData->nacionality,
                                'email'             => $valueData->email,
                                'bank'              => $valueData->bank,
                                'phone'             => $valueData->phone,
                                'biweekly_salary'   => $valueData->biweekly_salary,
                                'acconunt_number'    => $valueData->acconunt_number,
                                'acconunt_clabe'     => $valueData->acconunt_clabe,
                                'line'              => $iteration,
                                'status'            => 'Listo para relacionar a la empresa'
                            ];
                            
                        } 
                    
                    }else if ( in_array($valueData->phone, array_column($phones, 'phone') ) )
                    {
                        /*
                        *Saves users who have an unsigned site code in the DB
                        */
                        $contenedor_users[] = [
                            'rfc'               => $valueData->rfc,
                            'name'              => $valueData->name,
                            'nacionality'       => $valueData->nacionality,
                            'email'             => $valueData->email,
                            'bank'              => $valueData->bank,
                            'phone'             => $valueData->phone,
                            'biweekly_salary'   => $valueData->biweekly_salary,
                            'acconunt_number'    => $valueData->acconunt_number,
                            'acconunt_clabe'     => $valueData->acconunt_clabe,
                            'line'              => $iteration,
                            'status'            => 'El telefono ya existe en la base de datos'
                        ];
                        $container=1;

                    }else if ( !in_array($valueData->bank, array_column($banks, 'id') ) )
                    {
                        /*
                        *Saves users who have an unsigned site code in the DB
                        */
                        $contenedor_users[] = [
                            'rfc'               => $valueData->rfc,
                            'name'              => $valueData->name,
                            'nacionality'       => $valueData->nacionality,
                            'email'             => $valueData->email,
                            'bank'              => $valueData->bank,
                            'phone'             => $valueData->phone,
                            'biweekly_salary'   => $valueData->biweekly_salary,
                            'acconunt_number'    => $valueData->acconunt_number,
                            'acconunt_clabe'     => $valueData->acconunt_clabe,
                            'line'              => $iteration,
                            'status'            => 'El id del banco no esta registrado'
                        ];
                        //return ['message' => 'El empleado se ha creado con exito'];
                        $container=1;
                         
                    }else{

                        /*
                        *Saves users who have an unsigned site code in the DB
                        */
                        $contenedor_users[] = [
                            'rfc'               => $valueData->rfc,
                            'name'              => $valueData->name,
                            'nacionality'       => $valueData->nacionality,
                            'email'             => $valueData->email,
                            'bank'              => $valueData->bank,
                            'phone'             => $valueData->phone,
                            'biweekly_salary'   => $valueData->biweekly_salary,
                            'acconunt_number'    => $valueData->acconunt_number,
                            'acconunt_clabe'     => $valueData->acconunt_clabe,
                            'line'              => $iteration,
                            'status'            => 'Usuario listo para registrar'
                        ];

                    }

                    $iteration++;
                }//close foreach

                if ($container==1) {
                
                return [  
                    'message' => 'La operacion no se realizo',
                    'customer_users' => $contenedor_users,
                ];                
            }
            Log::info("Casi termino");

            $customer = Customer::where('admin_id',Auth::user()->id)->first();

            foreach ($contenedor_users as $value) {

                if ($value['status'] == 'Usuario listo para registrar') {
                    
                    try {
                    
                        DB::beginTransaction();
                        //$user = User::create([$valueData]);
                        $pass = substr( md5(microtime()), 1, 8);
                        /*
                        *querry  the id table user
                        */
                        $user = User::create([
                            'rfc'           => $value['rfc'],
                            'name'          => $value['name'],
                            'nacionality'   => $value['nacionality'],
                            'email'         => $value['email'],
                            'password'      => $pass,
                            'role'          => 'empleado',

                        ]);
                        
                        CustomerUser::create([
                            'customer_id'       => $customer->id,
                            'user_id'           => $user->id,
                            'bank_id'           => $value['bank'],
                            'phone'             => $value['phone'],
                            'biweekly_salary'   => $value['biweekly_salary'],
                            'acconunt_number'    => $value['acconunt_number'],
                            'acconunt_clabe'    => $value['acconunt_clabe'],
                        ]);

                        DB::commit();   
                    } catch (Exception $e) {
                        DB::rollBack();
                    }
                    
                }else if($value['status'] == 'Listo para relacionar a la empresa'){
                    
                    try {
                        DB::beginTransaction();
                        $user = User::select('id')->where('rfc',$value['rfc'])->first();
            
                        CustomerUser::create([
                            'customer_id'       => $customer->id,
                            'user_id'           => $user->id,
                            'bank_id'           => $value['bank'],
                            'phone'             => $value['phone'],
                            'biweekly_salary'   => $value['biweekly_salary'],
                            'acconunt_number'    => $value['acconunt_number'],
                            'acconunt_clabe'    => $value['account_clabe'],
                        ]);  

                    DB::commit();   
                    } catch (Exception $e) {
                        DB::rollBack();
                    }
                }
                
            }//close foreach

            return [  
                'message' => 'La operacion de registro se realizo',
                'customer_users' => $contenedor_users,
            ];
        }//close else
    
       return true;
        
    }//close methods import

}
