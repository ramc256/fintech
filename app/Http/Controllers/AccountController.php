<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Account;
use App\Bank;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.account.index'); 
    }

    public function list()
    {
        Log::info("controlller account list: ");
        return [
        	'accounts' 	=> Account::with('bank')->orderBy('id', 'DESC')->paginate(),
        	'banks' 	=> Bank::all()
        ];
    }

    public function store()
    {

        $account = Account::create(request()->all());
        $account->save();

        return ['message' => 'El registro se realizo con exito'];
    }

    public function update($id)
    {
        $account = Account::find($id);

        $account->fill(request()->all());

        $account->save();
        
        return ['message' => 'La actualizacion se realizo con exito'];
    }

    public function show()
    {
        return view('admin.account.show') -> with(['id' => request() -> id]);
    }

    public function getData()
    {
       return Account::FindOrFail(request()->id);
    }

    public function delete()
    {
        Account::destroy(request()->id);

        return ['message' => 'Se ha eliminado con exito'];
    }
}
