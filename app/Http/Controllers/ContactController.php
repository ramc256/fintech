<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('admin.contact.index'); 
    }

    public function store()
    {

		Mail::to("admin@fintech.com")->send(new \App\Mail\NewCustomer($data));

        return ['message' => 'El registro se realizo con exito'];
    }
}
