<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use App\CustomerUser;
use Mail;
use Log;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.customer.index'); 
    }

    public function list()
    {   
        return Customer::search(request()->search, request() ->status )->paginate();
    }

    public function create()
    {
        return view('auth.register_customer');
    }

    public function store()
    {

        $customer = Customer::create(request()->all());

        if (request()->register) {

            $data = [
                'name' => $customer->name,
                'rfc' => $customer->rfc,
                'address' => $customer->address,
                'cp_first_name' => $customer ->cp_first_name,
                'cp_last_name' => $customer ->cp_last_name,
                'cp_email' => $customer->cp_email,
                'cp_phone' => $customer->cp_phone,
            ];
            Mail::to("admin@fintech.com")->send(new \App\Mail\NewCustomer($data));
            Mail::to($customer->cp_email)->send(new \App\Mail\CustomerNotification($data));

            $data = nulL;
            //flash('La solicitud de registro fue exitosa')->success();
            return redirect()->back();

        }

        return ['message' => 'La empresa se ha creado con exito'];
    }

     public function register()
    {
        $customer = Customer::create(request()->all());

        $data = [
            'name' => $customer->name,
            'rfc' => $customer->rfc,
            'address' => $customer->address,
            'cp_first_name' => $customer ->cp_first_name,
            'cp_last_name' => $customer ->cp_last_name,
            'cp_email' => $customer->cp_email,
            'cp_phone' => $customer->cp_phone,
        ];

        Mail::to("admin@fintech.com")->send(new \App\Mail\NewCustomer($data));

        $data = nulL;

        if (request()->register) {
            return redirect()->route('home.customer');
        }

        return ['message' => 'La empresa se ha creado con exito'];
    }


    // carga el vector de usuarios que pueden ser adminsitradores
    public function getUsers()
    {
        $customers_users = CustomerUser::where('customer_id', request()->id)->first();
        $users = User::where('id', $customers_users->user_id)->first();

        return $users;
    }

    public function update($id)
    {
        $customer = Customer::find($id);

        $customer->fill(request()->all());

        $customer->save();

        if( request()->status=='verificado'){

            $data = [
                'name' => $customer->name,
                'rfc' => $customer->rfc,
                'address' => $customer->address,
                'cp_first_name' => $customer ->cp_first_name,
                'cp_last_name' => $customer ->cp_last_name,
                'cp_email' => $customer->cp_email,
                'cp_phone' => $customer->cp_phone,
            ];

            Mail::to($customer->cp_email)->send(new \App\Mail\CustomerVerificated($data));

            $data = nulL;

        }
        
        return ['message' => 'La empresa se ha actualizada con exito'];
    }

    public function show()
    {
        return view('admin.customer.show') -> with(['id' => request() -> id]);
    }

    public function getData()
    {
       return Customer::FindOrFail(request()->id);
    }

    public function delete()
    {
        $customer = Customer::find(request()->id); 
        $customer -> deleted = true; 
        $customer -> save();

        return ['message' => 'La empresa se ha eliminado con exito'];
    }
}
