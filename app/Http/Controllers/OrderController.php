<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Assets\ResourceFunctions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\CustomerUser;
use App\Order;
use App\User;
use App\Credit;
use App\Message;
use App\Account;
use App\Bank;
use Response;
use Twilio;
use Twilio\Twiml;
use Twilio\Rest\Client;
use Carbon\Carbon;
use Auth;	


class OrderController extends Controller
{
    public function getMessage(Request $request)
    {

    	Log::info("recibio la peticion del nuemero de telefono: ".$request->from);
      	/*
		*generate array via the separator " " which is the one with the txt file
		*/	
        $data= explode(" ", $request->text );
		$date=ResourceFunctions::rangeDate();	
		                
		/**
      	* [$customerUsers description: query that looks for the telephone number of the request]
      	* @var [type]
      	*/
      	$customerUser = CustomerUser::select('*')
	    				 ->where('phone', $request->from)
	    				 ->first();

	    Message::create([
            'phone' 	=> $request->from,
            'body'  	=> $request->text            
        ]);
    
	    if ( !empty($customerUser) ) {
			
			$credit = Order::with('credit')->where(
				['orders.customer_user_id'=> $customerUser->id])

					  ->whereBetween('orders.created_at', [ $date[0], $date[1]])
					  ->get();
					  
      		$result= ResourceFunctions::validateCredit($customerUser, $credit, $data[2]);
      		
      		if (( stristr($data[0] , 'PRESTAN') === false ) && ( stristr($data[0] , 'BALANCES') === false  ) ) {
      			ResourceFunctions::sendMessage($request->from, "La primera palabra del mensaje debe ser PRESTAN o BALANCES" );
	        
	        }else if ($customerUser->customer->name != $data[1]) {
	        	ResourceFunctions::sendMessage($request->from, "Indicaste un nombre de empresa incorrecto" );
	        
	        }else if( !(stristr($data[0] , 'BALANCES') === false) ){
	        	
	        	ResourceFunctions::sendMessage($request->from, "Tiene un saldo disponible de: ".$result['available'] );	
	        
	        }else if ( !is_numeric($data[2])) {
	        	ResourceFunctions::sendMessage($request->from, "En el monto debe incluir solo numeros");
	        
	        }else if ( $result['flag']	 ){
	        	ResourceFunctions::sendMessage($request->from, $result['message'] );	
	        
	        }else{

	        	try {
	        		DB::beginTransaction();
		        	
		        	$order = Order::create([
		                'customer_user_id' => $customerUser->id,
		                'amount'           => $data[2],
		            ]);

		            $credit = Credit::create([
		                'order_id' 	=> $order->id,
		                'status'    => 'pending_processing',
		            ]);
		            
		            if ( ($result['available'] - $data[2]) <= 0) {
		            	$available_balance= 0.00;
		            }else{
		            	$available_balance = ($result['available'] - $data[2]);
		            }
		           	ResourceFunctions::sendMessage($request->from, "La solicitud se proceso con exito, Tiene un saldo disponible de: ".$available_balance);
		           	
	        	DB::commit();   
                } catch (Exception $e) {
                    DB::rollBack();
                }
	        	
	        }
        }else{
        	//dd("solicitud no procede",$request->from);
        		ResourceFunctions::sendMessage($request->from, "solicitud no procede");

        } 

		$xml = '<Response> <Message>Hello. Neat, eh?</Message>></Response>';
	    $response = Response::make($xml, 200);
	    $response->header('Content-Type', 'text/xml');
	    return $response;
	     

    }
}
