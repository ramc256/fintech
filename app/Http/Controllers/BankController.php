<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Bank;

class BankController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.bank.index'); 
    }

    public function list()
    {
        Log::info("listo los bancos: ");
        return Bank::search(request()->search)->orderBy('id', 'DESC')->paginate();
    }

    public function store()
    {

        $bank = Bank::create(request()->all());
        $bank->save();

        return ['message' => 'El registro se realizo con exito'];
    }

    public function update($id)
    {
        $bank = Bank::find($id);

        $bank->fill(request()->all());

        $bank->save();
        
        return ['message' => 'La actualizacion se realizo con exito'];
    }

    public function show()
    {
        return view('admin.bank.show') -> with(['id' => request() -> id]);
    }

    public function getData()
    {
       return Bank::FindOrFail(request()->id);
    }

    public function delete()
    {
        Bank::destroy(request()->id);

        return ['message' => 'Se ha eliminado con exito'];
    }

}
